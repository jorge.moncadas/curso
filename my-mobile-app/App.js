
import { SafeAreaView, ScrollView, StyleSheet, Text, View, StatusBar } from "react-native";
import { Button } from "react-native";
import Calculadora from "./components/calculadora";
import Contador from "./components/contador";

export default function App() {
  console.log("Iniciando aplicacion");
  return (
    <>
      <StatusBar style="auto" />
      <Calculadora/>

    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
  safeContainer: {
    flex: 1,
    paddingTop: StatusBar.currentHeight,
  },
});
