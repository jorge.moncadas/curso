import {
  Text,
  View,
  Button,
  StyleSheet,
  Image,
  ScrollView,
  TouchableOpacity,
} from "react-native";
import { useState } from "react";

export default function Calculadora(props) {
  const [pantalla, setPantalla] = useState(props.init ?? "0");
  const [limpiar, setLimpiar] = useState(props.init ?? true);
  const [acumulado, setAcumulado] = useState(props.init ?? 0);
  let resultadoAux = 0;
  const [operador, setOperador] = useState("+");

  const ponDigito = (value) => {
    // console.log("value = "+value)
    // console.log("pantalla = "+pantalla)
    // console.log("objetivo = "+ pantalla+value)
    if (typeof value !== "string") value = value.toString();
    if (value.length != 1 || value < "0" || value > "9") {
      console.error("No es un valor numérico.");
      return;
    }
    if (limpiar || pantalla === "0") {
      setPantalla(value);
      setLimpiar(false);
    } else {
      setPantalla(pantalla + value);
    }
  };

  const ponComa = () => {
    if (limpiar) {
      setPantalla("0.");
      setLimpiar(false);
    } else if (pantalla.indexOf(".") === -1) {
      setPantalla(pantalla + ".");
    } else {
      console.warn("Ya está la coma");
    }
  };

  const inicia = () => {
    setAcumulado(0);
    setOperador("+");
    setPantalla("0");
    setLimpiar(true);
  };

  const cambiaSigno = () => {
    setPantalla((-pantalla).toString());
    if (limpiar) {
      setAcumulado(-acumulado);
    }
  };

  const calcula = (value) => {
    // console.log(value)
    // console.log(pantalla)
    // console.log(acumulado)

    if ("+-x/%=".indexOf(value) === -1) return;

    let operando = parseFloat(pantalla);
    resultadoAux = acumulado;
    //console.log("operador: " + operador);
    switch (operador) {
      case "+":
        resultadoAux = acumulado + operando;
        break;
      case "-":
        resultadoAux = acumulado - operando;
        break;
      case "x":
        resultadoAux = acumulado * operando;
        break;
      case "/":
        resultadoAux = acumulado / operando;
        break;
      case "%":
        resultadoAux = acumulado % operando;
        break;
      case "=":
        break;
    }

    setAcumulado(resultadoAux);
    setPantalla(parseFloat(resultadoAux.toPrecision(15)).toString());
    setOperador(value);
    setLimpiar(true);
  };

  const styles = StyleSheet.create({
    resultContainer: {
      backgroundColor: "#3a3a3c",
      maxWidth: "100%",
      minHeight: "25%",
      alignItems: "flex-end",
      justifyContent: "flex-end",
    },
    resultText: {
      maxHeight: 45,
      color: "white",
      margin: 15,
      fontSize: 35,
    },
    buttonsContainer: {
      width: "100%",
      maxHeight: "42%",
      flexDirection: "row",
      flexWrap: "wrap",
    },
    button: {
      borderColor: "#3a3a3c",
      borderStyle: 'solid',
      borderWidth: 1,
      alignItems: "center",
      justifyContent: "center",
      minWidth: "24%",
      minHeight: "54%",
      flex: 2,
    },
    number: {
      backgroundColor: "#8e8e93",
    },
    textButton: {
      color: "white",
      fontSize: 28,
    },
  });

  return (
    <View style={{}}>
      <View style={styles.resultContainer}>
        <Text style={styles.resultText}>{pantalla}</Text>
      </View>
      <View style={styles.buttonsContainer}>
        <TouchableOpacity
          style={[styles.button, { backgroundColor: "#636366" }]}
          onPress={() => inicia()}
        >
          <Text style={[styles.textButton]}>AC</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={[styles.button, { backgroundColor: "#636366" }]}
          onPress={() => cambiaSigno()}
        >
          <Text style={styles.textButton}>+/-</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={[styles.button, { backgroundColor: "#636366" }]}
          onPress={() => calcula("%")}
        >
          <Text style={styles.textButton}>%</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={[styles.button, { backgroundColor: "#FF9933" }]}
          onPress={() => calcula("/")}
        >
          <Text style={styles.textButton}>/</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={[styles.button, styles.number]}
          onPress={() => ponDigito(7)}
        >
          <Text style={styles.textButton}>7</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={[styles.button, styles.number]}
          onPress={() => ponDigito(8)}
        >
          <Text style={styles.textButton}>8</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={[styles.button, styles.number]}
          onPress={() => ponDigito(9)}
        >
          <Text style={styles.textButton}>9</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={[styles.button, { backgroundColor: "#FF9933" }]}
          onPress={() => calcula("x")}
        >
          <Text style={styles.textButton}>x</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={[styles.button, styles.number]}
          onPress={() => ponDigito(4)}
        >
          <Text style={styles.textButton}>4</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={[styles.button, styles.number]}
          onPress={() => ponDigito(5)}
        >
          <Text style={styles.textButton}>5</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={[styles.button, styles.number]}
          onPress={() => ponDigito(6)}
        >
          <Text style={styles.textButton}>6</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={[styles.button, { backgroundColor: "#FF9933" }]}
          onPress={() => calcula("-")}
        >
          <Text style={styles.textButton}>—</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={[styles.button, styles.number]}
          onPress={() => ponDigito(1)}
        >
          <Text style={styles.textButton}>1</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={[styles.button, styles.number]}
          onPress={() => ponDigito(2)}
        >
          <Text style={styles.textButton}>2</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={[styles.button, styles.number]}
          onPress={() => ponDigito(3)}
        >
          <Text style={styles.textButton}>3</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={[styles.button, { backgroundColor: "#FF9933" }]}
          onPress={() => calcula("+")}
        >
          <Text style={styles.textButton}>+</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={[styles.button, styles.number, { minWidth: "49%" }]}
          onPress={() => ponDigito(0)}
        >
          <Text style={styles.textButton}>0</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={[styles.button, styles.number]}
          onPress={() => ponComa()}
        >
          <Text style={styles.textButton}>,</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={[styles.button, { backgroundColor: "#FF9933" }]}
          onPress={() => calcula("=")}
        >
          <Text style={styles.textButton}>=</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}
