import { Text, View, Button, StyleSheet, Image, ScrollView } from "react-native";
import { useState } from "react";

export default function Contador(props) {
  const [contador, setContador] = useState(props.init ?? 0);

  const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: "#fff",
      alignItems: "center",
      justifyContent: "center",
    },
    contadorContainer: {
      flexDirection: "row",
      flexWrap: "wrap",
    },
  });

  return (
    <View style={styles.container}>
      <Text>{contador}</Text>
      <View style={styles.contadorContainer}>
        <Button title="Baja" onPress={() => setContador(contador - 1)}></Button>
        <Button title="Sube" onPress={() => setContador(contador + 1)}></Button>
      </View>
      <Image
        source={{
          uri: "https://reactnative.dev/img/tiny_logo.png",
          width: 40,
          height: 40,
        }}
      />
      <Image
        source={{
          uri: "data:image /png;base64,iVBORw0KGgoAAAANSUhEUgAAADMAAAAzCAYAAAA6oTAqAAAAEXRFWHRTb2Z0d2FyZQBwbmdjcnVzaEB1SfMAAABQSURBVGje7dSxCQBACARB+2/ab8BEeQNhFi6WSYzYLYudDQYGBgYGBgYGBgYGBgYGBgZmcvDqYGBgmhivGQYGBgYGBgYGBgYGBgYGBgbmQw+P/eMrC5UTVAAAAABJRU5ErkJggg==",
          width: 40,
          height: 40,
        }}
      />
    </View>
  );
}
