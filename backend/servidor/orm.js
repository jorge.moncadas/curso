const { Sequelize, DataTypes, Op, QueryTypes, where } = require('sequelize');
const initModels = require("./models/init-models");
const sequelize = new Sequelize('mysql://root:root@localhost:3306/autenticacion')
const dbContext = initModels(sequelize);

async function getUsuario(id) {
    let row = await dbContext.Usuarios.findOne({ where: { idUsuarios: { [Op.eq]: id } } })
    return row
}

async function changePassword(id, newPass) {
    let row = await getUsuario(id)
    if(row){
        row.password = newPass
        await row.save()
    }
    return row
}

module.exports.getUsuario = getUsuario;
module.exports.changePassword = changePassword;