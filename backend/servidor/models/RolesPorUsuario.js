const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('RolesPorUsuario', {
    idUsuarios: {
      type: DataTypes.STRING(200),
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'Usuarios',
        key: 'idUsuarios'
      }
    },
    idRol: {
      type: DataTypes.STRING(50),
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'Roles',
        key: 'idRol'
      }
    }
  }, {
    sequelize,
    tableName: 'RolesPorUsuario',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "idUsuarios" },
          { name: "idRol" },
        ]
      },
      {
        name: "FK_RolesPorUsuario_Roles",
        using: "BTREE",
        fields: [
          { name: "idRol" },
        ]
      },
    ]
  });
};
