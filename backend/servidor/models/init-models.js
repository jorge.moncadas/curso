var DataTypes = require("sequelize").DataTypes;
var _Roles = require("./Roles");
var _RolesPorUsuario = require("./RolesPorUsuario");
var _Usuarios = require("./Usuarios");

function initModels(sequelize) {
  var Roles = _Roles(sequelize, DataTypes);
  var RolesPorUsuario = _RolesPorUsuario(sequelize, DataTypes);
  var Usuarios = _Usuarios(sequelize, DataTypes);

  Roles.belongsToMany(Usuarios, { as: 'idUsuarios_Usuarios', through: RolesPorUsuario, foreignKey: "idRol", otherKey: "idUsuarios" });
  Usuarios.belongsToMany(Roles, { as: 'idRol_Roles', through: RolesPorUsuario, foreignKey: "idUsuarios", otherKey: "idRol" });
  RolesPorUsuario.belongsTo(Roles, { as: "idRol_Role", foreignKey: "idRol"});
  Roles.hasMany(RolesPorUsuario, { as: "RolesPorUsuarios", foreignKey: "idRol"});
  RolesPorUsuario.belongsTo(Usuarios, { as: "idUsuarios_Usuario", foreignKey: "idUsuarios"});
  Usuarios.hasMany(RolesPorUsuario, { as: "RolesPorUsuarios", foreignKey: "idUsuarios"});

  return {
    Roles,
    RolesPorUsuario,
    Usuarios,
  };
}
module.exports = initModels;
module.exports.initModels = initModels;
module.exports.default = initModels;
