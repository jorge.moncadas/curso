var express = require("express");
var router = express.Router();

// function suma(a, b) {
//   return a + b;
// }
// module.exports = suma;

router.get("/", function (req, res, next) {
  res.send("Es una demo").end()
});

router.get("/cotilla/:id", function (req, res, next){
  res.send(`
    id: ${req.params.id}\n
    page: ${req.query.page || "no sabe"}\n
    row: ${req.query.row || "no sabe"}\n
    user: ${req.body.user || "no sabe"}\n
    pass: ${req.body.pass || "no sabe"}\n
  `)
  res.json({
    "id": req.params.id,
    "page": req.query.page || "no sabe",
    "row": req.query.row || "no sabe",
    "user": req.body.user || "no sabe",
    "pass": req.body.pass || "no sabe",
  }).end()

  res.format({
    'text/plain': function () { 
      res.send(`
      id: ${req.params.id}\n
      page: ${req.query.page || "no sabe"}\n
      row: ${req.query.row || "no sabe"}\n
      user: ${req.body.user || "no sabe"}\n
      pass: ${req.body.pass || "no sabe"}\n
     `) },
    'application/json': function () { res.json({
      "id": req.params.id,
      "page": req.query.page || "no sabe",
      "row": req.query.row || "no sabe",
      "user": req.body.user || "no sabe",
      "pass": req.body.pass || "no sabe",
    }) },
    'default': function () { res.status(406).send('Not Acceptable') }
    })
});

module.exports = router;


// function suma(a, b) {
//   return a + b;
// }
// module.exports = suma;