import React from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  addNotificacion,
  deleteNotificacion,
  initNotificaciones,
  selectNotificacion,
} from "./store/notificaciones-slice";

export default function Notificaciones() {
  const notificaciones = useSelector(selectNotificacion);
  const dispatch = useDispatch();

  let elements = [];

  elements = notificaciones.map((el, index) => (
    <div key={index}>
      <p>
        {el}
        <button
          className="ms-3 btn btn-danger btn-sm"
          onClick={() => {
            dispatch(deleteNotificacion(index));
          }}
        >
          Eliminar
        </button>
      </p>
    </div>
  ));

  if(elements.length === 0) return
  
  return (
    <div className="p-3 w-50 mx-auto" align="center">
      <div>{elements}</div>
      <button
        className="btn btn-success m-2"
        onClick={() => {
          dispatch(addNotificacion("Notificacion Añadida :)"));
        }}
      >
        {" "}
        Añadir notificacion
      </button>
      <button
        className="btn btn-primary m-2"
        onClick={() => {
          dispatch(
            initNotificaciones([
              "Notificacion Nueva 1:)",
              "Notificacion Nueva 2:)",
              "Notificacion Nueva 3:)",
            ])
          );
        }}
      >
        {" "}
        Reiniciar notificaciones
      </button>
    </div>
  );
}
