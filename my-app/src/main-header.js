import { Container, Nav, Navbar,  } from 'react-bootstrap';
import { NavLink, Link } from 'react-router-dom';
import logo from './logo.svg';

export default function MainHeader() {
  return (
    <header>
      <Navbar bg="primary" variant="dark" expand="lg">
        <Container>
          <Navbar.Brand><Link to="/"><img src={logo} height="50" alt="logo" /></Link></Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="me-auto">
              <Nav.Item><NavLink className="nav-link" to="/">Inicio</NavLink></Nav.Item>
              <Nav.Item><NavLink className="nav-link" to="/calculadora">Calculadora</NavLink></Nav.Item>
              <Nav.Item><NavLink className="nav-link" to="/muro">Muro</NavLink></Nav.Item>
              <Nav.Item><NavLink className="nav-link" to="/formulario">Formulario</NavLink></Nav.Item>
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    </header>
  )
}
