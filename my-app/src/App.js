import "./App.css";
import { Calculadora } from "./calculadora";
//import "./calculadora.css";
import Demos from "./ejemplos";
import Formulario from "./formularios";
import { Muro } from "./muro";
import "./muro.css";
import logo from "./logo.svg";
import {
  BrowserRouter as Router,
  Routes,
  Route,
  Link,
  BrowserRouter,
} from "react-router-dom";
import Notificaciones from "./notificaciones-stored";
import MainHeader from "./main-header";

function App() {
  return (
    <>
      <BrowserRouter>
        <Notificaciones />
        <MainHeader />
        <Routes>
          <Route path="/" element={<Demos />}></Route>
          <Route path="/calculadora" element={<Calculadora />}></Route>
          <Route path="/formulario" element={<Formulario />}></Route>
          <Route path="/muro" element={<Muro />}></Route>
        </Routes>
        {/* <Formulario /> */}
      </BrowserRouter>
      {/* <Calculadora />
      <Muro /> */}
    </>
  );
}

export default App;
