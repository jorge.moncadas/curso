import { configureStore } from '@reduxjs/toolkit';
import newContadorReducer from './contador-slice'
import notificacionesReducer from './notificaciones-slice'

export default configureStore({
    reducer: {
      cont: newContadorReducer,
      notificaciones: notificacionesReducer,
    },
  });
