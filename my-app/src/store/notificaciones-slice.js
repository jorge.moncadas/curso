import { createSlice } from '@reduxjs/toolkit';

const initialState = { list: ["notificacion 1", 'notificacion 2']};

export const notificacionesSlice = createSlice({
    name: 'notificaciones',
    initialState,
    reducers: {
        addNotificacion: (state, action) => { state.list.push(action.payload) },
        deleteNotificacion: (state, action) => { state.list.splice(action.payload, 1) },
        init: (state, action) => {
            state.list = action.payload.list;
        },
    },
}); 

export const { addNotificacion, deleteNotificacion, init } = notificacionesSlice.actions;

export const selectNotificacion= (state) => state.notificaciones.list;

export const initNotificaciones = (list) => (dispatch, getState) => {
    dispatch(init({ list }));
};

export const defaultInitNotificaciones = () => (dispatch, getState) => {
    dispatch(init(initialState));
};

export default notificacionesSlice.reducer;
