import React from "react";
//import './muro.css'


export class Boton extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isImage: false,
    };
    this.img = "https://picsum.photos/id/"+ props.index +"/800/800";
  }

  cambiar = (e) => {
    this.setState((prev) => {
      return { isImage: !prev.isImage };
    });
  };

  render() {
    if (!this.state.isImage) {
      //console.log("Entra IF")
      return (
        <button
          style={{ width: this.props.tamaño+'px', height: this.props.tamaño+'px' }}
          onClick={this.cambiar}
        >
          {this.props.index}
        </button>
      );
    } else {
      //console.log("Entra ELSE")

      if (this.props.tamaño >= 400) {
        this.img = "https://picsum.photos/id/"+ this.props.index +"/1200/720";
        return (
          <>
            <div
              className="card"
              style={{ width: this.props.tamaño+'px', height: this.props.tamaño+'px' }}
            >
              <img
                className="card-img-top"
                src={this.img}
                style={{ height: (this.props.tamaño * 0.6)+'px' }}
                alt="Imagen aleatoria"
              />
              <div
                className="card-body"
                style={{ height: (this.props.tamaño * 0.4)+'px' }}
              >
                <h5 className="card-title">Card title</h5>
                <p className="card-text">
                  Some quick example text to build on the card title and make up
                  the bulk of the card's content.
                </p>
                <a href="#" className="btn btn-primary">
                  Go somewhere
                </a>
              </div>
            </div>
          </>
        );
      } else {
        this.img = "https://picsum.photos/id/"+ this.props.index +"/800/800";
        return (

            <>
            <div
              className="card"
              style={{ width: this.props.tamaño+'px', height: this.props.tamaño+'px' }}
            >
              <img
                className="card-img-top"
                src={this.img}
                style={{ height: this.props.tamaño+'px'}}
                alt="Imagen aleatoria"
              />
            </div>
          </>
        );
      }
    }
  }
}

export class Muro extends React.Component {
  constructor(props) {
    super(props);
    this.number = 100;
    this.state = {
      tamaño: 100,
    };
  }

  baja = (e) => {
    e.preventDefault();
    this.setState((prev) => {
      return { tamaño: prev.tamaño - 50 };
    });
  };

  sube = (e) => {
    e.preventDefault();
    this.setState((prev) => {
      return { tamaño: prev.tamaño + 50 };
    });
  };

  render() {
    let elements = [];

    elements = [...Array(this.number)].map((el, index) => (
      <Boton key={index} index={index+1} tamaño={this.state.tamaño} />
    ));

    return (
      <>
        <div>
          <h1>{this.state.tamaño}</h1>
          <div style={{ textAlign: "center" }}>
            <button onClick={this.baja}>Disminuye</button>&nbsp;
            <button onClick={this.sube}>Aumenta</button>
          </div>
        </div>

        <div className="elements-container">{elements}</div>
      </>
    );
  }
}
