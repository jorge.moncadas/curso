import React from "react";
import './calculadora.css'

export class Calculadora extends React.Component {
  constructor(props) {
    super(props);
    this.operador = "+";
    this.acumulado = 0;
    this.state={
        pantalla: '0'
    }   
    this.limpiar = true;
  }

  ponDigito = (e) => {
    e.preventDefault();
    //console.log(e.target.textContent)
    let value = e.target.textContent;

    if (typeof value !== "string") value = value.toString();
    if (value.length != 1 || value < "0" || value > "9") {
      console.error("No es un valor numérico.");
      return;
    }
    if (this.limpiar || this.pantalla == "0") {
      this.setState((prev, props) => {
        return { pantalla: value };
      });
      this.limpiar = false;
    } else this.setState((prev, props) => {
        return { pantalla: prev.pantalla + value };
      });

  };

  ponComa = (e) => {
    e.preventDefault();

    if (this.limpiar) {
      this.setState((props) => {
        return { pantalla: "0." };
      });
      this.limpiar = false;
    } else if (this.state.pantalla.indexOf(".") === -1) {
      this.setState((prev) => {
        return { pantalla: prev.pantalla + '.' };
      });
    }else{console.warn("Ya está la coma");}
  };

  inicia =  (e) => {
    e.preventDefault();
		this.acumulado = 0;
		this.operador = '+';
		this.setState(() => {
      return { pantalla: '0' };
    });
		this.limpiar = true;
	};

  cambiaSigno =  (e) => {
    e.preventDefault();
		this.setState((prev) => {
      return { pantalla: (-prev.pantalla).toString()}
    });
		if (this.limpiar) {
			this.acumulado = -this.acumulado;
	  }

  };


  calcula = (e) => {
    e.preventDefault();
    const value = e.target.textContent
    if ("+-x/%=".indexOf(value) == -1) return;

    let operando = parseFloat(this.state.pantalla);
    switch (this.operador) {
      case "+":
        this.acumulado += operando;
        break;
      case "-":
        this.acumulado -= operando;
        break;
      case "x":
        this.acumulado *= operando;
        break;
      case "/":
        this.acumulado /= operando;
        break;
      case "%":
        this.acumulado = this.acumulado % operando;
        break;
    }
    // Number: double-precision IEEE 754 floating point.
    // 9.9 + 1.3, 0.1 + 0.2, 1.0 - 0.9
    this.pantalla = parseFloat(this.acumulado.toPrecision(15)).toString();
    this.setState(() => {
        return { pantalla: parseFloat(this.acumulado.toPrecision(15)).toString() };
      });
    // miPantalla = acumulado.toString();
    this.operador = value;
    this.limpiar = true;
  
  };

  render() {
    return (
      <>
        <h1>CALCULADORA</h1>
        <div className="calc-container">
          <output type="text">{this.state.pantalla}</output>
          <div className="buttons-container">
            <button id="ac" className="operador" onClick={this.inicia}>
              AC
            </button>
            <button className="operador" onClick={this.cambiaSigno}>+/-</button>
            <button className="operador" onClick={this.calcula}>%</button>
            <button className="operador" onClick={this.calcula}>
              /
            </button>
            <button className="number" onClick={this.ponDigito}>
              7
            </button>
            <button className="number" onClick={this.ponDigito}>
              8
            </button>
            <button className="number" onClick={this.ponDigito}>
              9
            </button>
            <button className="operador" onClick={this.calcula}>
              x
            </button>
            <button className="number" onClick={this.ponDigito}>
              4
            </button>
            <button className="number" onClick={this.ponDigito}>
              5
            </button>
            <button className="number" onClick={this.ponDigito}>
              6
            </button>
            <button className="operador" onClick={this.calcula}>
              -
            </button>
            <button className="number" onClick={this.ponDigito}>
              1
            </button>
            <button className="number" onClick={this.ponDigito}>
              2
            </button>
            <button className="number" onClick={this.ponDigito}>
              3
            </button>
            <button className="operador" onClick={this.calcula}>
              +
            </button>
            <button id="zero" className="number" onClick={this.ponDigito}>
              0
            </button>
            <button className="number" onClick={this.ponComa}>
              .
            </button>
            <button id="equal" className="operador" onClick={this.calcula}>
              =
            </button>
          </div>
        </div>
      </>
    );
  }
  }
