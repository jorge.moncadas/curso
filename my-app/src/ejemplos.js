//export const Saluda = (props) => (<h1>Hola {props.nombre}</h1>)
//export const Saluda = ({nombre}) => (<h1>Hola {nombre}</h1>) //Desestructurado

import React from "react";
import PropTypes from "prop-types";
import logo from "./logo.svg";
import { selectNotificacion } from "./store/notificaciones-slice";
import Notificaciones from "./notificaciones-stored";

export class Saluda extends React.Component {
  constructor(props) {
    super(props);
    let { nombre, apellidos, edad } = props;
    this.nombre = nombre;
    this.apellidos = apellidos;
  }
  render() {
    return (
      <h1>
        Holaa {this.nombre} ({this.props.edad})
      </h1>
    );
  }
}

Saluda.propTypes = {
  nombre: PropTypes.string.isRequired,
  apellidos: PropTypes.string,
  edad: PropTypes.number,
};

Saluda.defaultProps = {
  nombre: "mundo",
  edad: 18,
};

export const Card = ({ title, children }) => (
  <div className="card">
    <img
      className="card-img-top"
      alt="Imagen de tarjeta"
      src={logo}
      width={160}
      height={160}
    />
    <div className="card-body">
      <h5 className="card-title">{title}</h5>
      <div className="card-text">{children}</div>
    </div>
  </div>
);

export class Contador extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      contador: +props.init,
      delta: +props.delta,
    };
    this.baja = this.baja.bind(this)
  }

  baja(e) {
    e.preventDefault();
    this.setState((prev, props) => {
      return { contador: prev.contador - prev.delta };
    });
  }

  sube = (e) => {
    e.preventDefault();
    this.setState((prev, props) => {
      return { contador: prev.contador + prev.delta };
    });
  };

  render() {
    return (
      <div>
        <h1>{this.state.contador}</h1>
        <p>
          <button onClick={this.sube}>Sube</button>&nbsp;
          <button onClick={this.baja}>Baja</button>
          {/* <button onClick={(ev) => this.baja(ev)}>Baja</button> */}
        </p>
      </div>
    );
  }
   
}

export default function Demos(){
    const element = (
        <>
          <h1>Hola Mundo!</h1>
          <h2>Que tal?</h2>
        </>
        );
        //let nombre= 'Jorge'
        const books = [
          {
            "author": "Chinua Achebe",
            "country": "Nigeria",
            "imageLink": "images/things-fall-apart.jpg",
            "language": "English",
            "link": "https://en.wikipedia.org/wiki/Things_Fall_Apart\n",
            "pages": 209,
            "title": "Things Fall Apart",
            "year": 1958
          },
          {
            "author": "Hans Christian Andersen",
            "country": "Denmark",
            "imageLink": "images/fairy-tales.jpg",
            "language": "Danish",
            "link": "https://en.wikipedia.org/wiki/Fairy_Tales_Told_for_Children._First_Collection.\n",
            "pages": 784,
            "title": "Fairy tales",
            "year": 1836
          },
          {
            "author": "Dante Alighieri",
            "country": "Italy",
            "imageLink": "images/the-divine-comedy.jpg",
            "language": "Italian",
            "link": "https://en.wikipedia.org/wiki/Divine_Comedy\n",
            "pages": 928,
            "title": "The Divine Comedy",
            "year": 1315
          }
        ]
      
        const listItems = books.map((book, indice)=>
          <li  key={indice}><a className="list-group-item list-group-item-action" href={book.link}><b>{book.title}</b> - <i>{book.author}</i> ({book.year})</a></li>
        );
        const list = <ul className="list-group">{listItems}</ul>;
      
        return (
          <div className="App">
            <header className="App-header">
              <img src={logo} className="App-logo" alt="logo" />
              <p>
                Edit <code>src/App.js</code> and save to reload.
              </p>
              <a
                className="btn btn-danger"
                href="https://reactjs.org"
                target="_blank"
                rel="noopener noreferrer"
              >
                Learn React
              </a>
              <h1> Secreto: {process.env.REACT_APP_SECRET}</h1>
            </header>
      
            

      
            {element}
            {/* <h3 className="alert-dark">Hola {nombre.toUpperCase()}!</h3> */}
            {list}
            <Saluda nombre="Don Manuel"/>
            <Saluda nombre="Palotes" />
            <Card title="Tarjeta" children="Contenido de la tarjeta."/>
            <Saluda />
            <Contador init={10} delta={2}/>
          </div>
        );
      
}
