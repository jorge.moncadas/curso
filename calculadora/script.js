class Operacion {

    constructor (op1=null, op2=null, operador=null){
        this.operando1Value = op1;
        this.operando2Value = op2;
        this.operadorValue = operador;
    }

    get operando1(){
        return this.operando1Value;
    }

    set operando1(value){
        this.operando1Value = value;
    }

    get operando2(){
        return this.operando2Value;
    }

    set operando2(value){
        this.operando2Value = value;
    }

    get operador(){
        return this.operadorValue;
    }

    set operador(value){
        this.operadorValue = value;
    }

    reset(){
        this.operando1Value = null;
        this.operando2Value = null;
        this.operadorValue = null;
        console.log(this)
    }

    calcular(){
        switch(this.operadorValue){
            case '+':
                return (+this.operando1 + +this.operando2)
            case '-':
                return (+this.operando1 - +this.operando2)
            case 'x':
                return (+this.operando1 * +this.operando2)
            case '/':
                return (+this.operando1 / +this.operando2)
            default:
                throw new Error('Error. Operador desconocido')
        }
    }

    
}

let op = new Operacion('1','2','/')
console.log(op.calcular())

// op = new Operacion()
// //console.log(op.calcular())
// op.operando1 = 5
// op.operando2 = 2
// op.operador = 'x'
// console.log(op.calcular())