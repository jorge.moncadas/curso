package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoApplication implements CommandLineRunner{

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

	@Autowired
	@Qualifier("new")
	Persona persona;
	
	@Override
	public void run(String... args) throws Exception {
		persona.Saluda();
		System.out.println("Hello world!");
		System.out.println("Bye bye world!");
		persona.Despide();
		
	}

}
