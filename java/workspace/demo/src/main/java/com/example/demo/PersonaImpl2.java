package com.example.demo;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Qualifier("new")
@Component
public class PersonaImpl2 implements Persona {

	@Override
	public void Saluda() {
		System.out.println("Hola!!!!!");
	}
	
	@Override
	public void Despide() {
		System.out.println("Adios!!!!!");
	}
	
}
