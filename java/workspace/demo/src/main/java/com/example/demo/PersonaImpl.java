package com.example.demo;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Qualifier("old")
@Component
public class PersonaImpl implements Persona {

	@Override
	public void Saluda() {
		System.out.println("Hola!");
	}
	
	@Override
	public void Despide() {
		System.out.println("Adios!");
	}
	
}
